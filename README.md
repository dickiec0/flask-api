# flask-api

A flask server which accepts POST requests with file content and saves them locally.

## Start the Flask server

Dev server running on port 5000

<code>
export FLASK_APP=server.py
export ROOT_DIRECTORY=./
python -m flask run
</code>

or (with custom binding and port)

<code>
python -m flask run
</code>

## Send a file example

<code>
curl -i -X POST -H "Content-Type: application/json" -d "{\"data\":\"$( base64 playbook.yml )\",\"path\":\"ios/playbook.yml\"}" http://127.0.0.1:5000/upload_file
</code>