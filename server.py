import base64
import json
import os

from flask import Flask, request

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!\n'


@app.route('/upload_file', methods=['POST'])
def upload_file():
    root_directory = os.getenv('ROOT_DIRECTORY')
    json_data = json.loads(request.data)

    path = json_data['path']
    data = json_data['data']

    decoded = base64.b64decode(data)

    file_path = root_directory + path

    if decoded:

        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        f = open(file_path, "wb+")
        f.write(decoded)
        f.close()

        import subprocess
        subprocess.call(['chown', 'apache:apache', file_path])

        return {
            'filename': file_path
        }
    else:
        return {
            'error': 'No data to write'
        }


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, debug=True)
